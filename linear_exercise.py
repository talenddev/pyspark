from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('lr_exercise').getOrCreate()

from pyspark.ml.regression import LinearRegression


data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Linear_Regression/cruise_ship_info.csv', inferSchema=True, header=True)

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler

from pyspark.ml.feature import StringIndexer

indexer = StringIndexer(inputCol='Cruise_line', outputCol="lineIndex")
indexed = indexer.fit(data).transform(data)

assembler = VectorAssembler(inputCols=['lineIndex', 'Age', 'Tonnage', 'passengers', 'length', 'cabins', 'passenger_density'], outputCol='features')

output = assembler.transform(indexed)

final_data = output.select('features', 'crew')

train_data, test_data = final_data.randomSplit([0.7,0.3])

lr = LinearRegression(labelCol = 'crew')

lr_model = lr.fit(train_data)

test_result = lr_model.evaluate(test_data)

test_result.rootMeanSquaredError

test_result.r2

from pyspark.sql.functions import corr

df.select(corr('crew','passengers')).show()

data.select(corr('crew','cabins')).show()