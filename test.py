# def my_func(name = 'leo'):
#     '''
#     comment
#     '''
#     print('hello '+ name)

# def square(x):
#     return x**2

# if 1==1:
#     my_func('2')
# d = square(30)

# print(d)

# def times_two(var):
#     return var*2

# print(times_two(10))

# lambda var: var*2

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('Basics').getOrCreate()

df = spark.read.json('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/people.json')

df.show()

df.printSchema()

df.describe().show()

from pyspark.sql.types import StructField,StringType,IntegerType,StructType

data_schema = [StructField('age', IntegerType(), True), 
               StructField('name', StringType(), False),
               StructField('desc', StringType(), True)
               ]

final_struc = StructType(fields= data_schema)

df = spark.read.json('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/people.json', schema=final_struc)

df.withColumn('double_age', df['age']*2).show()

df.withColumnRenamed('age', 'my_new_age').show()

df.createOrReplaceTempView('people')

results = spark.sql("SELECT * from PEOPLE where age=30")

results.show()