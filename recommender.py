from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('recommender').getOrCreate()

from pyspark.ml.recommendation import ALS
from pyspark.ml.evaluation import RegressionEvaluator


dataset = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Recommender_Systems/movielens_ratings.csv', header=True, inferSchema=True)

dataset.show()

dataset.describe().show()

training, test = dataset.randomSplit([0.8,0.2])

als = ALS(maxIter=5, regParam=0.01, userCol='userId', ratingCol='rating', itemCol='movieId')

model = als.fit(training)

predictions = model.transform(test)

predictions.show()

evaluator = RegressionEvaluator(metricName='rmse', labelCol='rating', predictionCol='prediction')

rmse = evaluator.evaluate(predictions)

print('RMSE')
print(rmse)

single_user = test.filter(test['userId']==3).select(['movieId', 'userId'])

recommendations = model.transform(single_user)

recommendations.orderBy('prediction', ascending=False).show()