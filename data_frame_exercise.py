from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('dfex').getOrCreate()

df = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_DataFrame_Project_Exercise/walmart_stock.csv', inferSchema=True, header=True)

df.columns

df.printSchema()

# df.select('Date', 'Open', 'High', 'Low', 'Close').show()
for row in df.head(5):
    print(row)
    print('\n')

df.describe().show()

from pyspark.sql.functions import format_number

result = df.describe()

result.select(result['summary'],
                     format_number(result['Open'].cast('float'), 2).alias('Open'),
                     format_number(result['High'].cast('float'), 2).alias('High'),
                     format_number(result['Low'].cast('float'), 2).alias('Low'),
                     format_number(result['Close'].cast('float'), 2).alias('Close'),
                     result['Volume'].cast('int').alias('Volume')
                     ).show()

new_df = df.select((df['High']/df['Volume']).alias('HV Ratio'))

new_df.show()

df.orderBy(df['High'].desc()).head(1)[0][0]

from pyspark.sql.functions import mean, max, min, count
 
df.select(mean(df['Close'])).show()

df.select(max(df['Volume']),min(df['Volume'])).show()

df.filter("Close < 60").count()

(df.filter("High > 80").count() / df.count() )* 100

from pyspark.sql.functions import corr

df.select(corr('High', 'Volume')).show()

from pyspark.sql.functions import dayofmonth, hour, dayofyear, month, year, weekofyear,format_number,date_format

df.groupBy(year("Date")).agg({'High':'max'}).show()

df.groupBy(month("Date")).agg({'Close':'avg'}).show()

mdf = df.withColumn('Month', month('Date'))
mdfavg = mdf.select(['Month', 'Close']).groupBy('Month').mean()
final = mdfavg.select('Month', 'avg(Close)').orderBy('Month')
final.show()