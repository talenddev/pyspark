from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('mytree').getOrCreate()

data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Tree_Methods/College.csv', inferSchema=True, header=True )
data.printSchema()

from pyspark.ml.feature import VectorAssembler

assembler = VectorAssembler(inputCols = ['Apps', 'Accept', 'Enroll', 'Top10perc', 'Top25perc', 'F_Undergrad', 'P_Undergrad', 'Outstate', 'Room_Board', 'Books', 'Personal', 'PhD', 'Terminal', 'S_F_Ratio', 'perc_alumni', 'Expend', 'Grad_Rate'], outputCol = 'features')

output = assembler.transform(data)

from pyspark.ml.feature import StringIndexer

indexer = StringIndexer(inputCol='Private', outputCol='PrivateIndex')

indexed_output = indexer.fit(output).transform(output)

final_data = indexed_output.select('features', 'PrivateIndex')

train_data, test_data = final_data.randomSplit([0.7,0.3])

from pyspark.ml import Pipeline
from pyspark.ml.classification import RandomForestClassifier, GBTClassifier, DecisionTreeClassifier

dtc = DecisionTreeClassifier(labelCol='PrivateIndex', featuresCol='features')
frc = RandomForestClassifier(numTrees=100, labelCol='PrivateIndex', featuresCol='features')
gbt = GBTClassifier(labelCol='PrivateIndex', featuresCol='features')

dtc_model = dtc.fit(train_data)
frc_model = frc.fit(train_data)
gbt_model = gbt.fit(train_data)

dtc_preds = dtc_model.transform(test_data)
frc_preds = frc_model.transform(test_data)
gbt_preds = gbt_model.transform(test_data)

dtc_preds.show()
frc_preds.show()
gbt_preds.show()

from pyspark.ml.evaluation import BinaryClassificationEvaluator

binary_eval = BinaryClassificationEvaluator(labelCol='PrivateIndex')

print('DTC')
binary_eval.evaluate(dtc_preds)

print('FRC')
binary_eval.evaluate(frc_preds)

binary_eval2 = BinaryClassificationEvaluator(labelCol='PrivateIndex', rawPredictionCol='prediction')

print('GBT')
binary_eval2.evaluate(gbt_preds)

from pyspark.ml.evaluation import MulticlassClassificationEvaluator

acc_eval = MulticlassClassificationEvaluator(metricName='accuracy', labelCol='PrivateIndex')

print('DTC ACCURACY:')
acc_eval.evaluate(dtc_preds)

print('FRC ACCURACY:')
acc_eval.evaluate(frc_preds)

print('GBT ACCURACY:')
acc_eval.evaluate(gbt_preds)
