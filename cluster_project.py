from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('cluster').getOrCreate()

from pyspark.ml.clustering import KMeans

dataset = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Clustering/hack_data.csv', header=True, inferSchema=True)


#################################

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler

assembler = VectorAssembler(inputCols=['Session_Connection_Time', 'Bytes Transferred', 'Kali_Trace_Used', 'Servers_Corrupted', 'Pages_Corrupted', 'WPM_Typing_Speed'], outputCol = 'features')

output = assembler.transform(dataset)

#################################

from pyspark.ml.feature import StandardScaler

scaler = StandardScaler(inputCol="features", outputCol="scaledFeatures", withStd=True, withMean=False)
scalerModel = scaler.fit(output)
cluster_final_data = scalerModel.transform(output)

#################################

from pyspark.ml.clustering import KMeans
kmeans1 = KMeans(featuresCol='scaledFeatures', k=2)
model1= kmeans1.fit(cluster_final_data)

kmeans2 = KMeans(featuresCol='scaledFeatures', k=3)
model2= kmeans2.fit(cluster_final_data)

results1 = model1.transform(cluster_final_data)
results1.groupby('prediction').count().show()

results2 = model2.transform(cluster_final_data)
results2.groupby('prediction').count().show()