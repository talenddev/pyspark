from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('log_reg').getOrCreate()

my_data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Logistic_Regression/titanic.csv', inferSchema=True, header=True)

my_data.show()

my_data.printSchema()

my_cols = my_data.select(['Survived', 'Pclass', 'Sex', 'Age', 'SibSp', 'Fare', 'Embarked'])

my_final_data = my_cols.na.drop()

from pyspark.ml.feature import VectorAssembler, VectorIndexer, OneHotEncoder, StringIndexer

gender_indexer = StringIndexer(inputCol='Sex', outputCol="SexIndex")
gender_encoder = OneHotEncoder(inputCol = 'SexIndex', outputCol='SexVec')
embarked_indexer = StringIndexer(inputCol='Embarked', outputCol="EmbarkIndex")
embarked_encoder = OneHotEncoder(inputCol = 'EmbarkIndex', outputCol='EmbarkVec')

assembler = VectorAssembler(inputCols=[ 'Pclass', 'SexVec', 'EmbarkVec', 'Age', 'SibSp', 'Fare'], outputCol='features')

from pyspark.ml.classification import LogisticRegression
from pyspark.ml import Pipeline

log_reg_titanic = LogisticRegression(featuresCol = 'features', labelCol='Survived')

pipeline = Pipeline(stages = [gender_indexer, embarked_indexer, gender_encoder, embarked_encoder, assembler,log_reg_titanic])

train_data, test_data = my_final_data.randomSplit([0.7,0.3])

fit_model = pipeline.fit(train_data)

results = fit_model.transform(test_data)

from pyspark.ml.evaluation import BinaryClassificationEvaluator

my_eval = BinaryClassificationEvaluator(rawPredictionCol = 'prediction', labelCol = 'Survived')

AUC = my_eval.evaluate(results)