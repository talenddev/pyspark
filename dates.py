from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('dates').getOrCreate()

df = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/appl_stock.csv', inferSchema=True, header=True)

df.head(1)

from pyspark.sql.functions import dayofmonth, hour, dayofyear, month, year, weekofyear,format_number,date_format

df.select(dayofmonth(df['Date'])).show()

df.select(hour(df['Date'])).show()

df.select(month(df['Date'])).show()

df.select(year(df['Date'])).show()

new_df = df.withColumn("Year", year(df['Date']))

new_df.show()

result = new_df.groupBy("Year").mean().select(["Year", "avg(Close)"])

result.show()

new_result= result.withColumnRenamed("avg(Close)", "Average Closing")

new_result.select(['Year',format_number('Average Closing',2).alias('Average')]).show()