from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('consult_tree').getOrCreate()

data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Tree_Methods/dog_food.csv', inferSchema=True, header=True )
data.printSchema()

from pyspark.ml.feature import VectorAssembler

assembler = VectorAssembler(inputCols = ['A', 'B', 'C', 'D'], outputCol = 'features')

output = assembler.transform(data)

final_data = output.select('features', 'Spoiled')

from pyspark.ml.classification import RandomForestClassifier,DecisionTreeClassifier

rfc = DecisionTreeClassifier(labelCol='Spoiled',featuresCol='features')

rfc_model = rfc.fit(final_data)

rfc_model.featureImportances