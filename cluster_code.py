from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('cluster').getOrCreate()

from pyspark.ml.clustering import KMeans

dataset = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Clustering/seeds_dataset.csv', header=True, inferSchema=True)

dataset.show()

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler

assembler = VectorAssembler(inputCols=dataset.columns, outputCol = 'features')

output = assembler.transform(dataset)

from pyspark.ml.feature import StandardScaler

scaler = StandardScaler(inputCol='features', outputCol='scaledFeatures')

scaler_model = scaler.fit(output)
final_data = scaler_model.transform(output)
final_data.head()

from pyspark.ml.clustering import KMeans
kmeans = KMeans(featuresCol='scaledFeatures', k=3)

model = kmeans.fit(final_data)

wsse = model.computeCost(final_data)

print(wsse)

centers = model.clusterCenters()
print(centers)

results = model.transform(final_data)

results.show()
