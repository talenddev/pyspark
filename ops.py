from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('ops').getOrCreate()


df = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/appl_stock.csv', inferSchema=True, header=True)

df.filter("Close < 500").show()

df.filter("Close < 500").select('Open').show()

df.filter(df['Close'] < 500).select('Open').show()

df.filter((df['Close'] < 200) & ~(df['Open']> 200)).select('Open').show()

df.filter(df['Low']== 197.16).show()

result = df.filter(df['Low']== 197.16).collect()

row = result[0]

row.asDict()
row.asDict()['Volume']