# Linear regression

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('lr_example').getOrCreate()

from pyspark.ml.regression import LinearRegression

# Load training data
training = spark.read.format("libsvm").load('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Linear_Regression/sample_linear_regression_data.txt')

training.show()

lr = LinearRegression(featuresCol='features', labelCol='label', predictionCol='prediction')

lrModel = lr.fit(training)

print("Coefficients: {}".format(str(lrModel.coefficients))) # For each feature...
print('\n')
print("Intercept:{}".format(str(lrModel.intercept)))

trainingSummary = lrModel.summary
trainingSummary.residuals.show()
print("RMSE: {}".format(trainingSummary.rootMeanSquaredError))
print("r2: {}".format(trainingSummary.r2))

all_data = spark.read.format("libsvm").load('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Linear_Regression/sample_linear_regression_data.txt')
train_data, test_data = all_data.randomSplit([0.7,0.3])


correct_model = lr.fit(train_data)
test_results = correct_model.evaluate(test_data)
test_results.residuals.show()

test_results.residuals.show()
print("RMSE: {}".format(test_results.rootMeanSquaredError))

unlabeled_data = test_data.select('features')

predictions = correct_model.transform(unlabeled_data)
predictions.show()

