from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('mytree').getOrCreate()

from pyspark.ml import Pipeline
from pyspark.ml.classification import RandomForestClassifier, GBTClassifier, DecisionTreeClassifier


data = spark.read.format('libsvm').load('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Tree_Methods/sample_libsvm_data.txt')

data.show()

train_data, test_data = data.randomSplit([0.7,0.3])

dtc = DecisionTreeClassifier ()
frc = RandomForestClassifier(numTrees=100)
gbt = GBTClassifier()

dtc_model = dtc.fit(train_data)
frc_model = frc.fit(train_data)
gbt_model = gbt.fit(train_data)

dtc_pred = dtc_model.transform(test_data)
frc_pred = frc_model.transform(test_data)
gbt_pred = gbt_model.transform(test_data)

dtc_pred.show()
frc_pred.show()
gbt_pred.show()

from pyspark.ml.evaluation import MulticlassClassificationEvaluator

acc_eval = MulticlassClassificationEvaluator(metricName='accuracy')

print('DTC ACCURACY:')
acc_eval.evaluate(dtc_pred)

print('FRC ACCURACY:')
acc_eval.evaluate(frc_pred)

print('GBT ACCURACY:')
acc_eval.evaluate(gbt_pred)

rfc_model.featureImportancesss