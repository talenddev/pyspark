from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('log_reg').getOrCreate()

from pyspark.ml.classification import LogisticRegression


my_data = spark.read.format('libsvm').load('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Logistic_Regression/sample_libsvm_data.txt')

my_data.show()

my_log_reg_model = LogisticRegression()

fitter_log_rel = my_log_reg_model.fit(my_data)

log_summary = fitter_log_rel.summary

log_summary.predictions.printSchema()

log_summary.predictions.show()

train_data, test_data = my_data.randomSplit([0.7,0.3])

my_log_reg_model = LogisticRegression()

fitter_log_rel = my_log_reg_model.fit(train_data)

predition_and_labels = fitter_log_rel.evaluate(test_data)

predition_and_labels.accuracy

predition_and_labels.predictions.show()



from pyspark.ml.evaluation import BinaryClassificationEvaluator,MulticlassClassificationEvaluator

my_eval = BinaryClassificationEvaluator()

my_final_roc = my_eval.evaluate(predition_and_labels.predictions)