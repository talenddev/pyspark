from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('miss').getOrCreate()

df = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/ContainsNull.csv', inferSchema=True, header=True)

df.show()

df.na.drop().show()

df.na.drop(thresh=2).show()

df.na.drop(how='any').show()

df.na.drop(how='all').show()

df.na.drop(subset=['Sales']).show()

df.printSchema()

df.na.fill('FILLED VALUE').show()

df.na.fill(0).show()

df.na.fill('No Name', subset=['Name']).show()

from pyspark.sql.functions import mean

mean_val = df.select(mean(df['Sales'])).collect()

mean_sales = mean_val[0][0]

df.na.fill(mean_sales, ['Sales']).show()

df.na.fill(df.select(mean(df['Sales'])).collect()[0][0], ['Sales']).show()