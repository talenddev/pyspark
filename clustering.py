from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('cluster').getOrCreate()

from pyspark.ml.clustering import KMeans

data = spark.read.format('libsvm').load('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Clustering/sample_kmeans_data.txt')

data.show()

final_data = data.select('features')

final_data.show()

kmeans = KMeans().setK(2).setSeed(1)

model= kmeans.fit(final_data)

wsse = model.computeCost(final_data)

print(wsse)

centers = model.clusterCenters()

results = model.transform(final_data)

results.show()


kmeans = KMeans().setK(3).setSeed(1)

model= kmeans.fit(final_data)

wsse = model.computeCost(final_data)

print(wsse)

centers = model.clusterCenters()

results = model.transform(final_data)

results.show()