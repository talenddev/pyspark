from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('aggs').getOrCreate()

df = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_DataFrames/sales_info.csv', inferSchema=True, header=True)

df.show()

df.groupBy("Company").mean().show()
df.groupBy("Company").min().show()
df.groupBy("Company").count().show()


df.agg({'Sales':'sum'}).show()
df.agg({'Sales':'max'}).show()

df.groupBy("Company").agg({'Sales':'max'}).show()

from pyspark.sql.functions import countDistinct, avg,stddev

df.select(countDistinct('Sales').alias('Average')).show()

from pyspark.sql.functions import format_number

sales_std = df.select(stddev('Sales').alias('std'))

sales_std.show()

sales_std.select(format_number('std',2)).show()

sales_std.select(format_number('std',2).alias('std')).show()

df.orderBy("Sales").show()

df.orderBy(df['Sales'].desc()).show()