from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('lr_example').getOrCreate()

from pyspark.ml.regression import LinearRegression


data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Linear_Regression/Ecommerce_Customers.csv', inferSchema=True, header=True)


data.printSchema()

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler

assembler = VectorAssembler(inputCols=['Avg Session Length', 'Time on App', 'Time on Website', 'Length of Membership'], outputCol='features')

output = assembler.transform(data)

final_data = output.select('features', 'Yearly Amount Spent')

train_data, test_data = final_data.randomSplit([0.7,0.3])

lr = LinearRegression(labelCol = 'Yearly Amount Spent')

lr_model = lr.fit(train_data)

test_result = lr_model.evaluate(test_data)

test_result.rootMeanSquaredError

test_result.r2

final_data.describe().show()


unlabeled_data = test_data.select('features')

predictions = lr_model.transform(unlabeled_data)

predictions.show()