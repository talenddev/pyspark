# Create seesion
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('log_reg_consult').getOrCreate()

# Load data
my_data = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Logistic_Regression/customer_churn.csv', inferSchema=True, header=True)
my_data.printSchema()

# Prepare data
from pyspark.ml.feature import VectorAssembler, VectorIndexer, OneHotEncoder, StringIndexer
assembler = VectorAssembler(inputCols=['Age', 'Total_Purchase', 'Account_Manager', 'Years', 'Num_Sites'], outputCol='features')

output = assembler.transform(my_data)
my_final_data = output.select('features', 'Churn')

# Split data
train_data, test_data = my_final_data.randomSplit([0.7,0.3])


# Set regression method
from pyspark.ml.classification import LogisticRegression
log_reg = LogisticRegression(featuresCol = 'features', labelCol='Churn')

# Create model
fit_model = log_reg.fit(train_data)

training_sum = fit_model.summary

training_sum.predictions.describe().show()

# Evaluate
from pyspark.ml.evaluation import BinaryClassificationEvaluator

results = fit_model.evaluate(test_data)

results.predictions.show()

my_eval = BinaryClassificationEvaluator(rawPredictionCol = 'prediction', labelCol = 'Churn')

auc = my_eval.evaluate(results.predictions)

final_lr_model = log_reg.fit(my_final_data)

new_customers = spark.read.csv('./Python-and-Spark-for-Big-Data-master/Spark_for_Machine_Learning/Logistic_Regression/new_customers.csv', inferSchema=True, header=True)
new_customers.printSchema()

test_new_customers = assembler.transform(new_customers)

final_results = final_lr_model.transform(test_new_customers)

final_results.select('Company','prediction').show()